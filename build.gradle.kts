import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.0"
}

group = "io.github.douglasgabriel"
version = "1.0-SNAPSHOT"

sourceSets {
    getByName("main") {
        output.resourcesDir = file("out")
    }
}

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    compile(kotlin("stdlib-jdk8"))
    compile("org.koin:koin-core:1.0.2")
    compile("org.jetbrains.exposed:exposed:0.11.2")
    compile("io.javalin:javalin:2.4.0")
    compile("org.slf4j:slf4j-simple:1.7.25")
    compile("com.h2database:h2:1.3.148")
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.2")
    compile("org.postgresql:postgresql:42.2.2")

    testCompile("org.koin:koin-test:1.0.2")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}