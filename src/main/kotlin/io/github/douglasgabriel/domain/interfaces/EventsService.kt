package io.github.douglasgabriel.domain.interfaces

import io.github.douglasgabriel.domain.entities.Event
import io.github.douglasgabriel.domain.interfaces.CrudService

interface EventsService: CrudService<Int, Event> {

    fun getByIssueNumber(number : Int): List<Event>

}