package io.github.douglasgabriel.domain.interfaces

/**
 * Defines CRUD methods for models.
 *
 * @param R the type of model primary key
 * @param T the type of model
 */
interface CrudService<R, T> {

    fun save(model: T): T
    /*fun update(model: T): T
    fun delete(id: R): T
    fun getAll(): Array<T>
    fun get(id: R): T*/

}