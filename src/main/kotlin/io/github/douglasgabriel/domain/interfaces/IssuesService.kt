package io.github.douglasgabriel.domain.interfaces

import io.github.douglasgabriel.domain.entities.Issue
import io.github.douglasgabriel.domain.interfaces.CrudService

interface IssueService: CrudService<Int, Issue>