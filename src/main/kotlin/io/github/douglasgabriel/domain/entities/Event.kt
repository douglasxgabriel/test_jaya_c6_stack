package io.github.douglasgabriel.domain.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class Event (
    val id: Int?,
    val action: String,
    @get:JsonIgnore
    val issue: Issue?
)