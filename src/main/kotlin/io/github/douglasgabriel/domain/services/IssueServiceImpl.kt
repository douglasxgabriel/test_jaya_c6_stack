package io.github.douglasgabriel.domain.services

import io.github.douglasgabriel.domain.entities.Issue
import io.github.douglasgabriel.resources.repositories.IssuesRepository
import io.github.douglasgabriel.domain.interfaces.IssueService

class IssueServiceImpl(
    private val issuesRepository: IssuesRepository
): IssueService {

    override fun save(model: Issue): Issue {
        return issuesRepository.save(model)
    }
}