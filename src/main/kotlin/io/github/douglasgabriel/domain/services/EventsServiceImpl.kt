package io.github.douglasgabriel.domain.services

import io.github.douglasgabriel.domain.entities.Event
import io.github.douglasgabriel.resources.repositories.EventsRepository
import io.github.douglasgabriel.domain.interfaces.EventsService
import io.github.douglasgabriel.domain.interfaces.IssueService

class EventsServiceImpl(
    private val eventsRepository: EventsRepository,
    private val issueService: IssueService
) : EventsService {

    override fun save(model: Event): Event {
        val issue = model.issue.let { issueService.save(it!!) }

        return eventsRepository.save(
            model.copy(issue = issue)
        )
    }

    override fun getByIssueNumber(number: Int): List<Event> {
        return eventsRepository.findByIssueNumber(number)
    }
}
