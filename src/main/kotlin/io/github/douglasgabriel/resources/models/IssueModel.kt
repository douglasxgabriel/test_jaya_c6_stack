package io.github.douglasgabriel.resources.models

import org.jetbrains.exposed.sql.Table

object IssueModel: Table() {
    val number = integer("number").primaryKey()
}