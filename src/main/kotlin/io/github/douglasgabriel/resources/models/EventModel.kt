package io.github.douglasgabriel.resources.models

import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Table

object EventModel: Table() {
    val id = integer("id").autoIncrement().primaryKey()
    val action = varchar("action", 10)
    val issueNumber = integer("issue_number").references(IssueModel.number).nullable()
}