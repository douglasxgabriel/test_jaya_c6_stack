package io.github.douglasgabriel.resources.repositories.implementations

import io.github.douglasgabriel.domain.entities.Issue
import io.github.douglasgabriel.resources.models.IssueModel
import io.github.douglasgabriel.resources.repositories.IssuesRepository
import org.jetbrains.exposed.sql.insertIgnore
import org.jetbrains.exposed.sql.transactions.transaction

class IssuesRepositoryImpl : IssuesRepository {

    override fun save(model: Issue): Issue {
        transaction {
            IssueModel.insertIgnore {
                it[number] = model.number
            }
        }

        return model.copy()
    }

}