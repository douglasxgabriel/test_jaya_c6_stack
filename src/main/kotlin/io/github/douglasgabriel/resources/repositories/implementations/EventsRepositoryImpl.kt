package io.github.douglasgabriel.resources.repositories.implementations

import io.github.douglasgabriel.domain.entities.Event
import io.github.douglasgabriel.resources.models.EventModel
import io.github.douglasgabriel.resources.repositories.EventsRepository
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.transaction

class EventsRepositoryImpl : EventsRepository {

    override fun save(model: Event): Event {
        return transaction {
            val modelId = EventModel.insert {
                it[action] = model.action
                it[issueNumber] = model.issue?.number
            } get EventModel.id

            model.copy(id = modelId)
        }
    }

    override fun delete(modelId: Int): Event? {
        val expression = EventModel.id eq modelId
        return transaction {
            val result = EventModel.select { expression }.map { convertToDTO(it) }.component1()

            EventModel.deleteWhere { expression }

            result
        }
    }

    override fun getAll(): List<Event> {
        return transaction {
            EventModel.selectAll().map { convertToDTO(it) }
        }
    }

    override fun get(modelId: Int): Event? {
        return transaction {
            EventModel.select { EventModel.id eq modelId }.map { convertToDTO(it) }.component1()
        }
    }

    override fun update(model: Event): Event? {
        return transaction {
            EventModel.update({ EventModel.id eq model.id!! }) {
                it[action] = model.action
                it[issueNumber] = model.issue?.number
            }

            EventModel.select { EventModel.id eq model.id!! }.map { convertToDTO(it) }.component1()
        }
    }

    override fun findByIssueNumber(number: Int) : List<Event> {
        var result : List<Event> = listOf()

        transaction {
            result = EventModel.select {
                EventModel.issueNumber eq number
            }.map { convertToDTO(it) }
        }

        return result
    }

    private fun convertToDTO(resultRow: ResultRow) : Event {
        return Event(
            id = resultRow[EventModel.id],
            action = resultRow[EventModel.action],
            issue = null
        )
    }

    private fun convertToModel(model : EventModel, dto : Event) {
        with(model) {
            TODO("Find a way to convert a DTO to a Model")
        }
    }
}