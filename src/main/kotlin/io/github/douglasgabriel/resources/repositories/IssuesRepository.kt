package io.github.douglasgabriel.resources.repositories

import io.github.douglasgabriel.domain.entities.Issue

interface IssuesRepository : CrudRepository<Int, Issue>