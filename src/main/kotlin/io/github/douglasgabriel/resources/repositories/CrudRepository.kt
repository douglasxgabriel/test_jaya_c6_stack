package io.github.douglasgabriel.resources.repositories

import io.github.douglasgabriel.domain.entities.Event

/**
 * @param T the type of model identifier
 * @param R the type of model
 */
interface CrudRepository<T, R> {

    fun save (model : R) : R
    fun delete (modelId : T) : R?
    fun getAll () : List<R>
    fun get (modelId : T) : R?
    fun update (model : R) : R?


}