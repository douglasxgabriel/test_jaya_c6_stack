package io.github.douglasgabriel.resources.repositories

import io.github.douglasgabriel.domain.entities.Event

interface EventsRepository : CrudRepository<Int, Event> {

    fun findByIssueNumber(number : Int): List<Event>

}