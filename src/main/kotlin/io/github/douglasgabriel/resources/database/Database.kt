package io.github.douglasgabriel.resources.database

import io.github.douglasgabriel.configurations.ConfigProp
import io.github.douglasgabriel.configurations.Environment
import io.github.douglasgabriel.resources.models.EventModel
import io.github.douglasgabriel.resources.models.IssueModel
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

class Database(
    private val environment: Environment
) {

    fun init() {
        Database.connect(
            environment[ConfigProp.DATABASE_URL]!!,
            driver = environment[ConfigProp.DATABASE_DRIVER]!!
        )

        transaction {
            SchemaUtils.create(
                IssueModel,
                EventModel
            )
        }
    }

}