package io.github.douglasgabriel

import io.github.douglasgabriel.application.config.api.Api
import io.github.douglasgabriel.di.appModule
import io.github.douglasgabriel.resources.database.Database
import org.koin.standalone.KoinComponent
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.inject

class Application : KoinComponent {
    val api by inject<Api>()
    val database by inject<Database>()

    fun start() {
        startKoin(listOf(appModule))
        database.init()
        api.init()
    }
}

fun main(args: Array<String>) {
    Application().start()
}