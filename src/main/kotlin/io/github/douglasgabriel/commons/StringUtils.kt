package io.github.douglasgabriel.commons

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

inline fun <reified T : Any> String.deserialize(): T? {
    return try {
        jacksonObjectMapper().readValue(this)
    } catch( ex : Exception ) {
        when(ex){
            is JsonParseException,
            is IllegalStateException -> null
            else -> throw ex
        }
    }
}