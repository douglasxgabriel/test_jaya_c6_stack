package io.github.douglasgabriel.di

/**
 * Contains module definitions.
 */

import io.github.douglasgabriel.application.config.api.Api
import io.github.douglasgabriel.configurations.Environment
import io.github.douglasgabriel.resources.database.Database
import io.github.douglasgabriel.application.web.controllers.EventsController
import io.github.douglasgabriel.application.web.controllers.IssuesController
import io.github.douglasgabriel.resources.repositories.EventsRepository
import io.github.douglasgabriel.resources.repositories.IssuesRepository
import io.github.douglasgabriel.resources.repositories.implementations.EventsRepositoryImpl
import io.github.douglasgabriel.resources.repositories.implementations.IssuesRepositoryImpl
import io.github.douglasgabriel.domain.interfaces.EventsService
import io.github.douglasgabriel.domain.interfaces.IssueService
import io.github.douglasgabriel.domain.services.EventsServiceImpl
import io.github.douglasgabriel.domain.services.IssueServiceImpl
import org.koin.dsl.module.module

val appModule = module {
    single { Database(get()) }
    single { IssueServiceImpl(get()) as IssueService }
    single { EventsServiceImpl(get(), get()) as EventsService }
    single { IssuesController(get()) }
    single { EventsController(get()) }
    single { Api(get(), get(), get()) }
    single { Environment() }
    single { EventsRepositoryImpl() as EventsRepository }
    single { IssuesRepositoryImpl() as IssuesRepository }
}