package io.github.douglasgabriel.configurations

import java.util.Properties

/**
 * Provides access to configurations defined in application configuration file.
 */
class Environment {

    val PROP_FILE = "application.properties"

    /**
     * Contains configurations defined in application configuration file.
     */
    private val properties by lazy {
        val properties = Properties()
        properties.load(
            ClassLoader.getSystemClassLoader().getResourceAsStream(PROP_FILE)
        )
        properties
    }

    /**
     * Get property value from application configuration file.
     * @param configProp property that must have its value returned.
     * @return the property value.
     */
    operator fun get(configProp : ConfigProp) : String? {
        return properties[configProp.propName].toString()
    }
}