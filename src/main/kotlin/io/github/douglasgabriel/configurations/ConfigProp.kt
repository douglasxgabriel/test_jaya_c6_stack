package io.github.douglasgabriel.configurations

/**
 * Contains keys to get configurations from Environment
 * @see Environment
 */
enum class ConfigProp(val propName : String) {
    API_PORT("api.port"),
    DATABASE_DRIVER("database.driver"),
    DATABASE_URL("database.url")
}