package io.github.douglasgabriel.application.config.api

import io.github.douglasgabriel.configurations.ConfigProp
import io.github.douglasgabriel.configurations.Environment
import io.github.douglasgabriel.application.web.controllers.EventsController
import io.github.douglasgabriel.application.web.controllers.IssuesController
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*

class Api(
    private val environment: Environment,
    private val issuesController : IssuesController,
    private val eventsController : EventsController
) : Javalin() {

    val apiPort by lazy {
        environment[ConfigProp.API_PORT]!!.toInt()
    }

    fun init() {
        routes {
            crud("events", eventsController)
        }


//        with(create().start(apiPort)) {
////            get("/issues/:id/events", issuesController.getEvents)
//
//            crud
//            post("/events", eventsController.post)
//        }
    }
}