package io.github.douglasgabriel.application.web.controllers

import io.github.douglasgabriel.domain.interfaces.CrudService
import io.javalin.Context
import io.javalin.apibuilder.CrudHandler
import org.eclipse.jetty.http.HttpStatus

abstract class AbstractCrudController<T, R : Any>(
    private val crudService: CrudService<T, R>
) : CrudHandler {

    override fun delete(ctx: Context, resourceId: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAll(ctx: Context) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getOne(ctx: Context, resourceId: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun update(ctx: Context, resourceId: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun create(ctx: Context) {
        val entity : R = deserialize(ctx)

        with(ctx) {
            json(crudService.save(entity))
            status(HttpStatus.CREATED_201)
        }
    }

    abstract fun deserialize(ctx: Context) : R
}