package io.github.douglasgabriel.application.web.controllers

import io.github.douglasgabriel.domain.entities.Event
import io.github.douglasgabriel.commons.deserialize
import io.github.douglasgabriel.domain.interfaces.EventsService
import io.javalin.BadRequestResponse
import io.javalin.Context
import org.eclipse.jetty.http.HttpStatus

class EventsController(
    eventsService: EventsService
) : AbstractCrudController<Int, Event>(eventsService) {

    override fun deserialize(ctx: Context): Event {
        return ctx.formParam("payload")?.deserialize() ?: throw BadRequestResponse()
    }
}