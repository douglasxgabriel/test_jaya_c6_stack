package io.github.douglasgabriel.application.web.controllers

import io.github.douglasgabriel.domain.interfaces.EventsService
import io.javalin.Context
import org.eclipse.jetty.http.HttpStatus

class IssuesController(
    private val eventsService: EventsService
) {

    val getEvents: (ctx: Context) -> Unit = {
        val issueNumber = it.pathParam("id").toInt()
        val result = eventsService.getByIssueNumber(issueNumber)

        if (result.count() > 0) it.json(result)
        else it.status(HttpStatus.NOT_FOUND_404)
    }

}